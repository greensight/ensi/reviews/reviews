<?php

namespace App\Domain\Common\Tests\Factories;

use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\CustomerGenderEnum;
use Ensi\CustomersClient\Dto\CustomerResponse;
use Ensi\CustomersClient\Dto\File;
use Ensi\LaravelEnsiFilesystem\Models\Tests\Factories\EnsiFileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CustomerFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'user_id' => $this->faker->modelId(),
            'manager_id' => $this->faker->nullable()->modelId(),
            'yandex_metric_id' => $this->faker->nullable()->uuid(),
            'google_analytics_id' => $this->faker->nullable()->uuid(),
            'status_id' => $this->faker->randomNumber(),
            'active' => $this->faker->boolean(),
            'email' => $this->faker->email(),
            'phone' => $this->faker->phoneNumber(),
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'middle_name' => $this->faker->firstName(),
            'gender' => $this->faker->nullable()->randomElement(CustomerGenderEnum::getAllowableEnumValues()),
            'create_by_admin' => $this->faker->boolean(),
            'avatar' => $this->faker->boolean() ? new File(EnsiFileFactory::new()->make()) : null,
            'city' => $this->faker->nullable()->city(),
            'birthday' => $this->faker->nullable()->dateTime(),
            'last_visit_date' => $this->faker->dateTime(),
            'comment_status' => $this->faker->nullable()->text(50),
            'timezone' => $this->faker->timezone(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Customer
    {
        return new Customer($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CustomerResponse
    {
        return new CustomerResponse([
            'data' => $this->make($extra),
        ]);
    }
}
