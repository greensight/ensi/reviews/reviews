<?php

namespace App\Domain\Common\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductResponse;
use Ensi\PimClient\Dto\ProductTypeEnum;

class ProductFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'external_id' => $this->faker->uuid(),
            'category_id' => $this->faker->modelId(),
            'brand_id' => $this->faker->modelId(),

            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(),
            'description' => $this->faker->text(50),
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'allow_publish' => $this->faker->boolean,
            'vendor_code' => $this->faker->numerify('######'),
            'barcode' => $this->faker->ean13(),

            'weight' => $this->faker->randomFloat(4),
            'weight_gross' => $this->faker->randomFloat(4),
            'length' => $this->faker->randomNumber(),
            'width' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'is_adult' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): Product
    {
        return new Product($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductResponse
    {
        return new ProductResponse(['data' => $this->make($extra)]);
    }
}
