<?php

namespace App\Domain\Reviews\Models;

use App\Domain\Reviews\Models\Tests\Factories\ReviewFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\ReviewStatusEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "Отзыв"
 *
 * @property int $id
 *
 * @property int $product_id - идентификатор товара
 * @property int $customer_id - пользователь, оставивший отзыв
 * @property string|null $comment - комментарий
 * @property int $grade - оценка (от 1 до 5)
 * @property ReviewStatusEnum $status_id - статус
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 */
class Review extends Model
{
    protected $table = 'reviews';

    protected $fillable = [
        'product_id',
        'customer_id',
        'comment',
        'grade',
        'status_id',
    ];

    protected $casts = [
        'status_id' => ReviewStatusEnum::class,
    ];

    public static function factory(): ReviewFactory
    {
        return ReviewFactory::new();
    }
}
