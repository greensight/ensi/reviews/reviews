<?php

namespace App\Domain\Reviews\Models\Tests\Factories;

use App\Domain\Reviews\Models\Review;
use App\Http\ApiV1\OpenApiGenerated\Enums\ReviewStatusEnum;
use Tests\Factories\BaseModelFactory;

class ReviewFactory extends BaseModelFactory
{
    protected $model = Review::class;

    public function definition(): array
    {
        return [
            'product_id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
            'comment' => $this->faker->optional()->text(255),
            'grade' => $this->faker->numberBetween(config('app.grade_min'), config('app.grade_max')),
            'status_id' => $this->faker->randomElement(ReviewStatusEnum::cases()),
        ];
    }

    public function isNew(): self
    {
        return $this->state(['status_id' => ReviewStatusEnum::NEW]);
    }
}
