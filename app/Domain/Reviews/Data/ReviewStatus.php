<?php

namespace App\Domain\Reviews\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\ReviewStatusEnum;

class ReviewStatus
{
    public string $name;

    public function __construct(public ReviewStatusEnum $status)
    {
        $this->fillNameById();
    }

    protected function fillNameById(): void
    {
        $this->name = match ($this->status) {
            ReviewStatusEnum::NEW => 'Новый',
            ReviewStatusEnum::PUBLISHED => 'Опубликован',
            ReviewStatusEnum::DENIED => 'Отклонен',
        };
    }

    public static function all(): array
    {
        $all = [];
        foreach (ReviewStatusEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }
}
