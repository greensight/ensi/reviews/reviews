<?php

namespace App\Domain\Reviews\Actions;

use App\Domain\Reviews\Models\Review;

class DeleteManyReviewsAction
{
    public function execute(array $productIds): void
    {
        Review::destroy($productIds);
    }
}
