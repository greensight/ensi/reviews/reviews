<?php

namespace App\Domain\Reviews\Actions;

use App\Domain\Reviews\Models\Review;

class PatchReviewAction
{
    public function execute(int $id, array $fields): Review
    {
        /** @var Review $review */
        $review = Review::query()->findOrFail($id);
        $review->fill($fields);
        $review->save();

        return $review;
    }
}
