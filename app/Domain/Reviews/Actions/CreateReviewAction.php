<?php

namespace App\Domain\Reviews\Actions;

use App\Domain\Reviews\Models\Review;
use App\Exceptions\BadRequestException;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\ApiException as CustomersApiException;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException as PimApiException;

class CreateReviewAction
{
    public function __construct(
        protected ProductsApi $productsApi,
        protected CustomersApi $customersApi
    ) {
    }

    public function execute(array $fields): Review
    {
        $this->validateCustomer($fields['customer_id']);
        $this->validateProduct($fields['product_id']);

        $review = new Review();
        $review->fill($fields);
        $review->save();

        return $review;
    }

    private function validateCustomer(int $customerId): void
    {
        try {
            $customer = $this->customersApi->getCustomer($customerId)->getData();
        } catch (CustomersApiException $e) {
            throw new BadRequestException("Ошибка при получении данных о клиенте, код ошибки {$e->getCode()}");
        }

        if (!$customer->getActive()) {
            throw new BadRequestException('Неактивный клиент не может оставлять отзывы');
        }
    }

    private function validateProduct(int $productId): void
    {
        try {
            $product = $this->productsApi->getProduct($productId)->getData();
        } catch (PimApiException $e) {
            throw new BadRequestException("Ошибка при получении данных о продукте, код ошибки {$e->getCode()}");
        }

        if (!$product->getAllowPublish()) {
            throw new BadRequestException('Нельзя оставлять отзывы на неактивные продукты');
        }
    }
}
