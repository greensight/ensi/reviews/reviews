<?php

namespace App\Domain\Reviews\Actions;

use App\Domain\Reviews\Models\Review;

class DeleteReviewAction
{
    public function execute(int $id): void
    {
        Review::destroy($id);
    }
}
