<?php

namespace App\Http\ApiV1\Modules\Reviews\Requests;

use App\Http\ApiV1\Support\Requests\MassDeleteRequest;

class MassDeleteReviewsRequest extends MassDeleteRequest
{
}
