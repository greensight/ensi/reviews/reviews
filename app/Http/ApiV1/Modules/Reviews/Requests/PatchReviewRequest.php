<?php

namespace App\Http\ApiV1\Modules\Reviews\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\ReviewStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchReviewRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'status_id' => ['nullable', 'integer', new Enum(ReviewStatusEnum::class)],
            'comment' => ['nullable', 'string', 'max:255'],
        ];
    }
}
