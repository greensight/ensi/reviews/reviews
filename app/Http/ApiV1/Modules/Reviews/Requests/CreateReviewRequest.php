<?php

namespace App\Http\ApiV1\Modules\Reviews\Requests;

use App\Domain\Reviews\Models\Review;
use App\Http\ApiV1\OpenApiGenerated\Enums\ReviewStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CreateReviewRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => [
                'required',
                Rule::unique(Review::class)->where('product_id', $this->product_id),
            ],
            'product_id' => ['required', 'integer'],

            'grade' => ['required', 'integer', 'between:' . config('app.grade_min') . ',' . config('app.grade_max')],
            'comment' => ['nullable', 'string', 'max:255'],

            'status_id' => ['required', 'integer', new Enum(ReviewStatusEnum::class)],
        ];
    }
}
