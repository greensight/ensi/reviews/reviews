<?php

namespace App\Http\ApiV1\Modules\Reviews\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\ReviewStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ReviewRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'product_id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
            'comment' => $this->faker->optional()->text(255),
            'grade' => $this->faker->numberBetween(config('app.grade_min'), config('app.grade_max')),
            'status_id' => $this->faker->randomElement(ReviewStatusEnum::cases()),
        ];
    }

    public function isNew(): self
    {
        return $this->state(['status_id' => ReviewStatusEnum::NEW]);
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
