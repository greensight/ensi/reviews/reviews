<?php

use App\Domain\Common\Tests\Factories\CustomerFactory;
use App\Domain\Common\Tests\Factories\ProductFactory;
use App\Domain\Reviews\Models\Review;
use App\Http\ApiV1\Modules\Reviews\Tests\Factories\ReviewRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\ReviewStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/reviews/reviews 201', function () {
    $request = ReviewRequestFactory::new()->isNew()->make();

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersCustomersApi()->allows([
        'getCustomer' => CustomerFactory::new()->makeResponse(['active' => true]),
    ]);
    $this->mockPimProductsApi()->allows([
        'getProduct' => ProductFactory::new()->makeResponse(['allow_publish' => true]),
    ]);

    postJson('/api/v1/reviews/reviews', $request)
        ->assertCreated();

    assertDatabaseHas(Review::class, [
        'product_id' => $request['product_id'],
        'customer_id' => $request['customer_id'],
        'grade' => $request['grade'],
        'status_id' => $request['status_id'],
    ]);
});

test('POST /api/v1/reviews/reviews 400 inactive product', function () {
    $request = ReviewRequestFactory::new()->isNew()->make();

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersCustomersApi()->allows([
        'getCustomer' => CustomerFactory::new()->makeResponse(['active' => true]),
    ]);
    $this->mockPimProductsApi()->allows([
        'getProduct' => ProductFactory::new()->makeResponse(['allow_publish' => false]),
    ]);

    postJson('/api/v1/reviews/reviews', $request)
        ->assertBadRequest();
});

test('POST /api/v1/reviews/reviews 400 unique product review for user', function () {
    $customerId = 1;
    $productId = 2;
    $request = ReviewRequestFactory::new()->isNew()->make(['customer_id' => $customerId, 'product_id' => $productId]);

    Review::factory()->isNew()->create(['customer_id' => $customerId, 'product_id' => $productId]);

    postJson('/api/v1/reviews/reviews', $request)
        ->assertBadRequest();
});

test('POST /api/v1/reviews/reviews 400', function () {
    $request = ReviewRequestFactory::new()->isNew()->make(['grade' => config('app.grade_max') + 1]);

    postJson('/api/v1/reviews/reviews', $request)
        ->assertBadRequest();
});

test('PATCH /api/v1/reviews/reviews/{id} 200', function () {
    /** @var Review $review */
    $review = Review::factory()->isNew()->create();
    $request = [
        'status_id' => ReviewStatusEnum::PUBLISHED->value,
        'comment' => 'PATCH test',
    ];

    patchJson("/api/v1/reviews/reviews/{$review->id}", $request)
        ->assertOk()
        ->assertJsonFragment($request);

    assertDatabaseHas(Review::class, $request);
});

test('PATCH /api/v1/reviews/reviews/{id} 404', function () {
    patchJson('/api/v1/reviews/reviews/404')
        ->assertNotFound();
});

test('GET /api/v1/reviews/reviews/{id} 200', function () {
    /** @var Review $review */
    $review = Review::factory()->create();

    getJson("/api/v1/reviews/reviews/{$review->id}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'product_id', 'customer_id', 'comment', 'grade', 'status_id', 'created_at', 'updated_at']]);
});

test('GET /api/v1/reviews/reviews/{id} 404', function () {
    getJson('/api/v1/reviews/reviews/404')
        ->assertNotFound();
});

test('DELETE /api/v1/reviews/reviews/{id} 200', function () {
    /** @var Review $reviewDelete */
    $reviewDelete = Review::factory()->create();
    /** @var Review $reviewOk */
    $reviewOk = Review::factory()->create();

    deleteJson("/api/v1/reviews/reviews/{$reviewDelete->id}")
        ->assertOk();

    assertDatabaseHas(Review::class, ['id' => $reviewOk->id]);
    assertDatabaseMissing(Review::class, ['id' => $reviewDelete->id]);
});

test('POST /api/v1/reviews/reviews:search 200', function () {
    $reviews = Review::factory()
        ->count(10)
        ->sequence(
            ['product_id' => 1],
            ['product_id' => 2],
        )
        ->create();

    $reviews = $reviews->filter(function ($review) {
        return $review->product_id == 2;
    })->sortBy([
        ['grade', 'desc'],
    ]);

    postJson('/api/v1/reviews/reviews:search', [
            'filter' => ['product_id' => 2],
            'sort' => ['-grade'],
        ])
        ->assertOk()
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $reviews->first()->id)
        ->assertJsonPath('data.0.product_id', 2);
});

test("POST /api/v1/reviews/reviews:search filter success", function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var Review $review */
    $review = Review::factory()->create($value ? [$fieldKey => $value] : []);
    Review::factory()->create();

    postJson("/api/v1/reviews/reviews:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $review->{$fieldKey}),
    ], 'sort' => ['created_at'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $review->id);
})->with([
    ['product_id', 1],
    ['status_id', ReviewStatusEnum::PUBLISHED],
    ['status_id', ReviewStatusEnum::PUBLISHED, 'status_id', [ReviewStatusEnum::PUBLISHED, ReviewStatusEnum::DENIED]],
    ['grade', env('GRADE_MIN', 1) + 1],
    ['grade', env('GRADE_MAX', 5), 'grade', [env('GRADE_MIN', 1), env('GRADE_MAX', 5)]],
    ['customer_id', 1],
    ['comment', 'my_comment', 'comment_like', 'my_'],
    ['created_at', '2023-03-20', 'created_at_gte', '2023-03-19'],
    ['created_at', '2023-03-20', 'created_at_lte', '2023-03-21'],
    ['updated_at', '2023-03-20', 'updated_at_gte', '2023-03-19'],
    ['updated_at', '2023-03-20', 'updated_at_lte', '2023-03-21'],
]);

test("POST /api/v1/reviews/reviews:search sort success", function (string $sort) {
    Review::factory()->create();
    postJson("/api/v1/reviews/reviews:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'product_id',
    'grade',
    'created_at',
    'updated_at',
]);

test('POST /api/v1/reviews/reviews:mass-delete 200', function () {
    /** @var Review $reviewDelete */
    $reviewDelete = Review::factory()->create();
    /** @var Review $reviewNoDelete */
    $reviewNoDelete = Review::factory()->create();
    $reviewIdNotExist = 404;

    $request = [
        'ids' => [$reviewDelete->id, $reviewIdNotExist],
    ];

    postJson('/api/v1/reviews/reviews:mass-delete', $request)
        ->assertOk();

    assertDatabaseHas(Review::class, ['id' => $reviewNoDelete->id]);
    assertDatabaseMissing(Review::class, ['id' => $reviewDelete->id]);
});
