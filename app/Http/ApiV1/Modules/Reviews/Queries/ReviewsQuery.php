<?php

namespace App\Http\ApiV1\Modules\Reviews\Queries;

use App\Domain\Reviews\Models\Review;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ReviewsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Review::query());

        $this->allowedSorts(['id', 'product_id', 'created_at', 'updated_at', 'grade']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('product_id'),
            AllowedFilter::exact('status_id'),
            AllowedFilter::exact('grade'),
            AllowedFilter::exact('customer_id'),

            ...StringFilter::make('comment')->contain(),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('-created_at');
    }
}
