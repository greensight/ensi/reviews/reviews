<?php

namespace App\Http\ApiV1\Modules\Reviews\Controllers;

use App\Domain\Reviews\Actions\CreateReviewAction;
use App\Domain\Reviews\Actions\DeleteManyReviewsAction;
use App\Domain\Reviews\Actions\DeleteReviewAction;
use App\Domain\Reviews\Actions\PatchReviewAction;
use App\Http\ApiV1\Modules\Reviews\Queries\ReviewsQuery;
use App\Http\ApiV1\Modules\Reviews\Requests\CreateReviewRequest;
use App\Http\ApiV1\Modules\Reviews\Requests\MassDeleteReviewsRequest;
use App\Http\ApiV1\Modules\Reviews\Requests\PatchReviewRequest;
use App\Http\ApiV1\Modules\Reviews\Resources\ReviewsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class ReviewsController
{
    public function create(CreateReviewRequest $request, CreateReviewAction $action): Responsable
    {
        return new ReviewsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchReviewRequest $request, PatchReviewAction $action): ReviewsResource
    {
        return new ReviewsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $reviewId, DeleteReviewAction $action): Responsable
    {
        $action->execute($reviewId);

        return new EmptyResource();
    }

    public function get(int $reviewId, ReviewsQuery $query): ReviewsResource
    {
        return new ReviewsResource($query->findOrFail($reviewId));
    }

    public function massDelete(MassDeleteReviewsRequest $request, DeleteManyReviewsAction $action): EmptyResource
    {
        $action->execute($request->getIds());

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, ReviewsQuery $query): Responsable
    {
        return ReviewsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
