<?php

namespace App\Http\ApiV1\Modules\Reviews\Resources;

use App\Domain\Reviews\Models\Review;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Review
 */
class ReviewsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,

            'product_id' => $this->product_id,
            'customer_id' => $this->customer_id,

            'comment' => $this->comment,
            'grade' => $this->grade,

            'status_id' => $this->status_id,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
