<?php

return [
    'catalog' => [
        'pim' => [
            'base_uri' => env('CATALOG_PIM_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'customers' => [
        'customers' => [
            'base_uri' => env('CUSTOMERS_CUSTOMERS_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
