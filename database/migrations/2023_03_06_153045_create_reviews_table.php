<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('product_id')->unsigned(); /** Идентификатор товара */
            $table->bigInteger('customer_id')->unsigned(); /** Пользователь, оставивший отзыв */
            $table->string('comment')->nullable(); /** Комментарий */
            $table->tinyInteger('grade')->unsigned(); /** Оценка (от 1 до 5) */
            $table->tinyInteger('status')->unsigned(); /** Статус публикации отзыва */

            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
};
