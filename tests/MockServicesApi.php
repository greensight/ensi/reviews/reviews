<?php

namespace Tests;

use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\PimClient\Api\ProductsApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Catalog ===============

    // region service Pim
    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }

    // endregion


    // =============== Customers ===============

    // region service Customers
    public function mockCustomersCustomersApi(): MockInterface|CustomersApi
    {
        return $this->mock(CustomersApi::class);
    }
    // endregion
}
