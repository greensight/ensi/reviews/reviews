<?php

namespace Tests\Factories;

use Ensi\LaravelTestFactories\FakerProvider;
use Faker\Generator;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @property Generator|FakerProvider $faker
 */
abstract class BaseModelFactory extends Factory
{
    protected function withFaker()
    {
        $faker = parent::withFaker();
        $faker->addProvider(new FakerProvider($faker));

        return $faker;
    }
}
